# Anotação

## tutorial basico de kafka
https://www.youtube.com/watch?v=PppMhofKzy4

## tutorial completo
https://www.youtube.com/playlist?list=PL5aY_NrL1rjt_AZxj11kQjiTNLGg4ZaZA

## docker compose 
https://github.com/confluentinc/cp-docker-images/blob/5.3.3-post/examples/kafka-cluster/docker-compose.yml

## como o kafka funciona
https://whimsical.com/kafka-EbWjeGL3gDg9apxewMyGhB

## trabalhando com protobuf
https://medium.com/@arcanine11/how-to-use-protobuf-with-golang-kafka-if-you-know-nothing-655d14eed35a


# comandos 
## acessar do container kafka
```shell
docker exec -it id_container sh|bash
```
## criar um tópico no kafka
```shell
kafka-topics --create --bootstrap-server localhost:19092 --replication-factor 3 --partitions 3 --topic products
```


## list os tópicos
```shell
kafka-topics --list --bootstrap-server localhost:19092
```

## conectar com um producer
```shell
kafka-console-producer --broker-list localhost:19092 --topic products
```

## conectar um consumer
```shell
kafka-console-consumer --bootstrap-server localhost:19092 --topic products
```

## detalhes do seu topico
```shell
kafka-topics --describe --bootstrap-server localhost:19092 --topic products
```

## atribuir um consumer a um grupo
```shell
kafka-console-consumer --bootstrap-server localhost:19092 --topic products --from-beginning --group product_all
```

##  detalhes do grupo de consumer
```shell
kafka-consumer-groups --group product_all --bootstrap-server localhost:19092 --describe
```
